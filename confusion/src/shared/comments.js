export const COMMENTS_ = [
  {
    id: 0,
    comment: 'Imagine all the eatables, living in conFusion!',
    author: '-- John Lemon, ',
    Date: 'Oct 17 2012',
  },
  {
    id: 1,
    comment:
      'Sends anyone to heaven, I wish I could get my mother-in-law to eat it!',
    author: '-- Paul McVites, ',
    Date: 'Sep 06 2014',
  },
  {
    id: 2,
    comment: 'Eat it, just eat it!',
    author: '-- Michael Jaikishan, ',
    Date: 'Feb 14 2015',
  },
  {
    id: 3,
    comment: 'Ultimate, Reaching for the stars!',
    author: '-- Ringo Starry, ',
    Date: 'Dec 03 2013',
  },
  {
    id: 4,
    comment: "Its your birthday, we're gonna party!",
    author: '-- 25 Cent, ',
    Date: 'Dec 03 2011',
  },
];

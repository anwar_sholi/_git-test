import React from 'react';
import {
  Card,
  CardImg,
  CardImgOverlay,
  CardTitle,
  Breadcrumb,
  BreadcrumbItem,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';

function Menu(props) {
  // render() {
  const menu = props.dishes.dishes.map((dish) => {
    return (
      <div key={dish.id} className='col-12 col-md-5 m-1'>
        <Card>
          <Link to={`/menu/${dish.id}`}>
            <CardImg width='100%' src={dish.image} alt={dish.name}></CardImg>
            <CardImgOverlay body className='ml-5'>
              <CardTitle>{dish.name}</CardTitle>
            </CardImgOverlay>
          </Link>
        </Card>
      </div>
    );
  });
  if (props.dishes.isLoading) {
    return (
      <div className='container'>
        <div className='row'>
          <Loading></Loading>
        </div>
      </div>
    );
  } else if (props.dishes.errMess) {
    return (
      <div className='container'>
        <div className='row'>
          <h4>{this.props.dishes.errMess}</h4>
        </div>
      </div>
    );
  } else
    return (
      <div>
        <div className='row'>
          <Breadcrumb>
            <BreadcrumbItem>
              <Link to='/home'>Home</Link>
            </BreadcrumbItem>
            <BreadcrumbItem active>Menu</BreadcrumbItem>
            <div className='col-12'>
              <h3>Menu</h3>
              <hr></hr>
            </div>
          </Breadcrumb>
        </div>
        <div className='row'>{menu}</div>
      </div>
    );
}
//}

export default Menu;

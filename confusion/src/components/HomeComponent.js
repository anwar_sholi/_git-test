import React from 'react';
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
} from 'reactstrap';
import { Loading } from './LoadingComponent';

function RenderCard({ item, isLoading, errMess }) {
  console.log(isLoading + errMess);
  if (isLoading) {
    return (
      <Loading>
        {''}
        {''}
      </Loading>
    );
  } else if (errMess) {
    return (
      <h4>
        {errMess}
        {''}
      </h4>
    );
  } else {
    if (item != null) {
      return (
        <Card>
          <CardImg src={item.image} alt={item.name} />
          <CardBody>
            <CardTitle>{item.name}</CardTitle>
            {item.designation ? (
              <CardSubtitle>{item.designation}</CardSubtitle>
            ) : null}
            ;<CardText>{item.description}</CardText>
          </CardBody>
        </Card>
      );
    } else {
      return <div></div>;
    }
  }
}

function Home(props) {
  return (
    <div className='container'>
      <div className='row align-items-start'>
        <div className='col-12 col-md m-1'>
          <RenderCard
            item={props.dishes}
            isLoading={props.dishesLoading}
            errMess={props.dishesErrMess}
          />
        </div>
        <div className='col-12 col-md m-1'>
          <RenderCard
            item={props.promotions}
            isLoading={props.dishesLoading}
            errMess={props.dishesErrMess}
          />
        </div>
        <div className='col-12 col-md m-1'>
          <RenderCard
            item={props.leaders}
            isLoading={props.dishesLoading}
            errMess={props.dishesErrMess}
          />
        </div>
      </div>
    </div>
  );
}
export default Home;

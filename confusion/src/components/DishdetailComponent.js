import {
  Card,
  CardBody,
  CardImg,
  CardText,
  CardTitle,
  Breadcrumb,
  BreadcrumbItem,
  Modal,
  ModalHeader,
  ModalBody,
  ModalTitle,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  Col,
  Row,
} from 'reactstrap';
import { COMMENTS_ } from '../shared/comments';
import { Link } from 'react-router-dom';
import React, { Component } from 'react';
import { Control, LocalForm, Errors } from 'react-redux-form';
import { Loading } from './LoadingComponent';

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !val || val.length <= len;
const minLength = (len) => (val) => val && val.length >= len;

function RenderDish({ dish }) {
  if (dish != null)
    return (
      <div className='col-12 col-md-5 m-1'>
        <Card>
          <CardImg top src={dish.image} alt={dish.name}></CardImg>
          <CardBody>
            <CardTitle>{dish.name}</CardTitle>
            <CardText>{dish.description}</CardText>
          </CardBody>
        </Card>
      </div>
    );
  else return <div></div>;
}
function RenderComments({ dish, comments, onClick }) {
  if (dish != null) {
    const allComments = comments.map((comment) => {
      return (
        <ul className='list-unstyled'>
          <li>{comment.comment}</li>
          <li>
            {comment.author}{' '}
            {new Intl.DateTimeFormat('en-US', {
              year: 'numeric',
              month: 'short',
              day: '2-digit',
            }).format(new Date(Date.parse(comment.date)))}
          </li>
        </ul>
      );
    });
    return (
      <div>
        <h4>Comments</h4>
        {allComments}
        <CommentForm onClick={onClick} />
      </div>
    );
  } else return <div></div>;
}
class CommentForm extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Button outline onClick={this.props.onClick}>
        <span className='fa fa-pencil fa-lg'></span> Submit Comment
      </Button>
    );
  }
}
class DishdetailComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCommentModalOpen: false,
    };
    this.toggleCommentModal = this.toggleCommentModal.bind(this);
    this.handleComment = this.handleComment.bind(this);
  }

  toggleCommentModal() {
    this.setState({
      isCommentModalOpen: !this.state.isCommentModalOpen,
    });
  }
  handleComment(values) {
    this.props.addComment(
      this.props.dish.id,
      values.rating,
      values.author,
      values.comment
    );
  }
  render() {
    if (this.props.isLoading) {
      return (
        <div className='container'>
          <div className='row'>
            <Loading />
          </div>
        </div>
      );
    } else if (this.props.errMess) {
      return (
        <div className='container'>
          <div className='row'>
            <h4>{this.props.errMess}</h4>
          </div>
        </div>
      );
    } else
      return (
        <React.Fragment>
          <div className='row'>
            <Breadcrumb>
              <BreadcrumbItem>
                <Link to='/menu'>Menu</Link>
              </BreadcrumbItem>
              <BreadcrumbItem active>{this.props.dish.name}</BreadcrumbItem>
              <div className='col-12'>
                <h3>{this.props.dish.name}</h3>
                <hr></hr>
              </div>
            </Breadcrumb>
          </div>
          <div className='row'>
            <RenderDish dish={this.props.dish} />
            <div className='col-12 col-md-5 m-1'>
              <RenderComments
                dish={this.props.dish}
                comments={this.props.comments}
                onClick={this.toggleCommentModal}
                addComment={this.props.addComment}
              />
            </div>
          </div>

          <Modal
            isOpen={this.state.isCommentModalOpen}
            toggle={this.toggleCommentModal}
          >
            <ModalHeader toggle={this.toggleCommentModal}>
              Submit Comment
            </ModalHeader>
            <ModalBody>
              <LocalForm onSubmit={(values) => this.handleComment(values)}>
                <Row className='form-group'>
                  <Label htmlFor='rating' md={12}>
                    Rating
                  </Label>
                  <Col md={12}>
                    <Control.select
                      model='.rating'
                      id='rating'
                      name='rating'
                      className='form-control'
                    >
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </Control.select>
                  </Col>
                </Row>
                <Row className='form-group'>
                  <Label htmlFor='author' md={12}>
                    Your Name
                  </Label>
                  <Col md={12}>
                    <Control.text
                      model='.author'
                      id='author'
                      name='author'
                      placeholder='your name'
                      className='form-control'
                      validators={{
                        minLength: minLength(3),
                        maxLength: maxLength(15),
                      }}
                    />
                    <Errors
                      className='text-danger'
                      model='.author'
                      show='touched'
                      messages={{
                        minLength: 'Must be greater than 2 characters',
                        maxLength: 'Must be 15 characters or less',
                      }}
                    />
                  </Col>
                </Row>
                <Row className='form-group'>
                  <Label htmlFor='comment' md={12}>
                    Comment
                  </Label>
                  <Col md={12}>
                    <Control.textarea
                      model='.comment'
                      id='comment'
                      name='comment'
                      rows='6'
                      className='form-control'
                    />
                  </Col>
                </Row>
                <Row className='form-group'>
                  <Col md={{ size: 10, offset: 0 }}>
                    <Button type='submit' color='primary'>
                      Submit
                    </Button>
                  </Col>
                </Row>
              </LocalForm>
            </ModalBody>
          </Modal>
        </React.Fragment>
      );
  }
}

export default DishdetailComponent;
